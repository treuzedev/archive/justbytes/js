#####
terraform {

  required_version = "~> 0.15.1"

  backend "s3" {
    bucket = "treuzedev-terraform"
    key    = "justbytes/js/state"
    region = "eu-west-1"
  }

}


#####
provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  region = "us-east-1"
  alias = "useast1"
}

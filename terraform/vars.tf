#####
variable "common_tags" {
  description = "Common tags applied to all resources. Defaults to {\"App\": \"justbytes\", \"Owner\": \"EMU\", \"Env\": \"Prod\"}"
  default = {
    "App" : "justbytes",
    "Owner" : "EMU",
    "Env" : "prod",
  }
}

// // // // //
// MAIN
// // // // //

// a function to send a file in chunks as a base64 encoded strings
// returns a promise that is fullfiled when upload is done
// file, url, packetSize=50, bytesPrefix='kb', syncSend=true, finalDestination='/tmp/justbytes'
function sendFile(params) {
    
    // checks if umber of arguments is correct
    checkNumberOfArguments(Function.length, arguments.length, 'sendFile');
    
    // checks and returns necessary parameters
    params = checkUserInput(params, 'blob');
    
    // convert from bytes into bytes
    // get number of chunks the file is going to be divided into
    const chunkSize = getChunkSize(params['chunkSize'], params['bytesPrefix']);
    const numberOfChunks = Math.ceil(params['input']['size'] / chunkSize);
    
    // send the file
    return new Promise(async resolve => {
        const uploadStatus = await sendFileAsMultipart(params['input'], chunkSize, numberOfChunks, params['url'], params['syncSend'], params['finalDestination']);
        resolve(uploadStatus);
    })
    .then(success => success)
    .catch(error => {
        throw new Error(error);
    });
    
} 

// a function to send a file after a user selects one
// return a promise that is resolved when the upload finishes after the user actually chooses a file to upload
function sendFileFromTag(params) {
    
    // checks number of arguments is correct
    checkNumberOfArguments(Function.length, arguments.length, 'sendFileFromTag');
    
    // checks and returns necessary parameters
    params = checkUserInput(params, 'tag');
    
    // convert from bytes prefix into bytes
    const chunkSize = getChunkSize(params['chunkSize'], params['bytesPrefix']);
    
    // when the user chooses a file, transform it into a base64 string of bytes
    // then remove the file from 
    // makes sure we are dealing with a file object
    // gets number of parts the file needs to be broken into
    // sends files as a multipart upload
    // removes file from memory
    // resolves promise
    return new Promise(resolve => {
        
        params['input'].onchange = async () => {
            
            const numberOfChunks = Math.ceil(params['input'].files[0]['size'] / chunkSize);
            
            const uploadStatus = await sendFileAsMultipart(params['input']['files'][0], chunkSize, numberOfChunks, params['url'], params['syncSend'], params['finalDestination']);
            
            params['input'].value = null;
            
            resolve(uploadStatus);
            
        };
        
    })
    .then(success => success)
    .catch(error => {
        throw new Error(error);
    });
    
}


// a function to convert bytes from a given metrics prefix
function getChunkSize(value, prefix) {
    if (prefix === 'kb') {
        return value * 1024;
    } else if (prefix === 'b') {
        return value;
    } else {
        throw new Error('Chunk size prefix needs to be one of kb or b.');
    }
}


// a function to divide a file into its necessary parts and send it to the specified destination
// if something wrong happens, then an error is thrown
// otherwise, returns a dict indicating that all went well
async function sendFileAsMultipart(file, chunkSize, numberOfChunks, url, syncSend, finalDestination) {
    
    // variables
    let chunk = 1;
    const tmpFolderName = Math.floor(Math.random() * 1000000).toString();
    
    // get filename and extension
    const filenameAndExtension = getFileNameAndExtension(file.name);
    const filename = filenameAndExtension['filename'];
    const extension = filenameAndExtension['extension'];
    
    // create a subblob from original file
    // convert that subblob into a base64 encoded string of bytes
    // send the string to the specified destination
    // loop for the specified number of chunks
    // if syncSend is false, send files without waiting for receival confirmation
    for (let i = 0; i < file['size']; i += chunkSize) {
        
        const subBlob = file.slice(i, (i + chunkSize));
        const base64String = await transformBlob(subBlob);
        
        const body = {
            'tmpFolderName': tmpFolderName,
            'filename': filename,
            'extension': extension,
            'finalDestination': finalDestination,
            'numberOfChunks': numberOfChunks,
            'chunk': chunk,
            'data': base64String,
        };
        
        if (syncSend) {
            
            let response = await fetch(url, {
                'method': 'POST',
                'headers': {
                    'Content-Type': 'application/json',
                },
                'body': JSON.stringify(body),
            });
            
            response = await response.json();
            
            if (response['status'] !== 200) {
                throw new Error(response['message']);
            }
            
        } else {
            
            try {
                
                fetch(url, {
                    'method': 'POST',
                    'headers': {
                        'Content-Type': 'application/json',
                    },
                    'body': JSON.stringify(body),
                });
                
            } catch (error) {
                
                throw new Error(error);
                
            }
            
        }
        
        chunk++;
        
    }
    
    // if no errors were raised, return an okay response
    return {
        'status': 200,
        'message': 'upload successful!',
        'syncSend': syncSend,
    };
    
}


// transform a file into a base64 string
// load the content as a binary string using the file reader object
// encode and return the string using base64 enconding
async function transformBlob(blob) {
    const binaryString = await readBlob(blob);
    return encodeString(binaryString);
}


// return file extension passing in a file name
function getFileNameAndExtension(filename) {
    
    // create regex
    const extensionRegex = /\..*$/;
    const filenameRegex = /^.*\./;
    
    // try to find extension and filename
    const extensionMatch = filename.match(extensionRegex);
    const filenameMatch = filename.match(filenameRegex);
    
    // return filename and extension, if file had an extension
    if (!extensionMatch) {
        return {
            'filename': filenameMatch[0].split('.')[0],
            'extension': '',
        };
    } else {
        return {
            'filename': filenameMatch[0].split('.')[0],
            'extension': extensionMatch[0].split('.')[1],
        };
    }
    
}


// read blob using file reader
function readBlob(blob) {
    
    // read the blob
    const reader = new FileReader();
    reader.readAsBinaryString(blob);
    
    // not used event listeners
    reader.abort = () => {};
    reader.onloadstart = () => {};
    reader.onloadend = () => {};
    reader.onprogress = () => {};
    
    // throw an error to the console if one occurs
    reader.onerror = (error) => {
        throw new Error(error);
    };
    
    // when file is loaded, return that string
    return new Promise(resolve => {
        reader.onload = (load) => {
            resolve(reader.result);
        };
    })
    .then(success => success)
    .catch(error => {
        throw new Error(error);
    });
    
}


// a function to encode a string using base64
function encodeString(string) {
    return window.btoa(string);
}


// a function that checks user input
// throws an error if something occurs
// returns the params otherwise with all required fields
function checkUserInput(params, type) {
    
    // checks if params is a map
    if (params === null || params === undefined) {
        throw new Error('params must not be null or undefined!');
    } else if (params.constructor !== Object) {
        throw new Error('params must be an object!');
    }
    
    // checks if there are extra keys
    const keys = Object.keys(params);
    
    for (let i = 0; i < keys.length; i++) {
        switch(keys[i]) {
            case "input":
                break;
            case "url":
                break;
            case "bytesPrefix":
                break;
            case "chunkSize":
                break;
            case "finalDestination":
                break;
            case "syncSend":
                break;
            default:
                throw new Error(`${keys[i]} is not a valid parameter!`);
        }
    }
    
    // if input is for sendFileFromTag
    // makes sure tag is an input one, and its type is file
    // makes sure there are no files loaded in the browser
    // if input is for sendFile
    // checks if input is, in fact a file or a blob
    // throws an error otherwise
    if (type === 'tag') {
        
        if (params['input']['tagName'] !== 'INPUT' || params['input']['type'] !== 'file') {
            throw new Error('HTML tag must be <input> and its type must be file.');
        }
        
        if (params['input']['files'].length !== 0) {
            throw new Error('The input tag MUST be empty. When a user chooses a file, it is then sent to the specified url.');
        }
        
    } else if (type === 'file' || type === 'blob') {
        
        if (!(params['input'] instanceof File) || !(params['input'] instanceof Blob)) {
            throw new Error('Object must be of type File or Blob.');
        }
        
    } else {
        throw new Error('Oh no! Something is wrong with the code!');
    }
    
    // makes sure an url was provided
    if (!params['url'] || !checkIfString(params['url'], 'url')) {
        throw new Error('url must be provided');
    }
    
    // make sure finaDestination is set and is a string
    // make sure it starts with a forward slash and does not have a forward slash in the end
    // if user did not provide a finalDestination, a default one is set
    if (params['finalDestination']) {
        
        if (checkIfString(params['finalDestination'], 'finalDestination')) {
            
            const regex = /^\/.*[^\/]$/;
            const match = params['finalDestination'].match(regex);
            
            if (!match) {
                throw new Error('finalDestination must start with a "/" and not end with a "/", for example, "/tmp/justbytes"');
            }
            
        }
        
    } else {
        params['finalDestination'] = '/tmp/justbytes';
    }
    
    // make sure syncSend is a boolean
    // defaults to true if syncSend was not defined or is null
    if (typeof params['syncSend'] !== 'boolean') {
        if (!params['syncSend']) {
            params['syncSend'] = true;
        } else {
            throw new Error('syncSend must be a boolean!');
        }
    }
    
    // make sure bytesPrefix is a string
    // getChunkSize checks if the prefix is valid
    // defaults to bytes
    if (params['bytesPrefix']) {
        checkIfString(params['bytesPrefix'], 'bytesPrefix');
    } else {
        params['bytesPrefix'] = 'b';
    }
    
    // make sure chunkSize is an int
    // defaults to 1024
    if (params['chunkSize']) {
        if (typeof params['chunkSize'] !== 'number') {
            throw new Error('chunkSize must be a safe int!');
        } else {
            if (!Number.isSafeInteger(params['chunkSize'])) {
                throw new Error('chunkSize must be a safe int!');
            }
        }
    } else {
        params['chunkSize'] = 1;
    }
        
    // return params with all necessary fields
    return params;
    
}

// checks if a variable is a string
function checkIfString(string, param) {
    if (typeof string !== 'string' || string.constructor !== String) {
        throw new Error(`${param} needs to be a string!`);
    } else {
        return true;
    }
}

// checks if number of arguments is correct
function checkNumberOfArguments(_function, _arguments, name) {
    if (_function !== _arguments) {
        throw new Error(`${name} expects ${_function} argument(s) but received ${_arguments}..`);
    }
}
